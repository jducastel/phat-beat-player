# Phat Beat Player

Audio python player for Piromoni's [phat beat](https://github.com/pimoroni/phat-beat) / pirate radio kit

Projet / roadmap :

1. lecture de fichiers locaux
  - Sur l'exemple de [pirate juke box](https://github.com/andywarburton/pirate_jukebox), lecture de fichiers mp3 d'un dossier.
  - lecture audio via [vlc + python](https://wiki.videolan.org/Python_bindings), [tuto](https://linuxconfig.org/how-to-play-audio-with-vlc-in-python), [tuto playlist 1](https://stackoverflow.com/questions/44229378/using-libvlc-media-list-instead-of-a-media-player-to-play-a-directory-of-songs), [tuto playlist 2](https://stackoverflow.com/questions/28440708/python-vlc-binding-playing-a-playlist/33042754)
  - contrôle via les boutons dédiés (on/off, play, pause, etc)
  - effets lumineux / usage des LED ?
  - Possibilité de basculer entre radio / fichiers mp3 ?
  - [lib phat beat](https://pypi.org/project/phatbeat/)


2. téléchargement d'une playlist deezer

- compte + profil deezer comme source de musique via [deezer-python](https://github.com/browniebroke/deezer-python) et/ou [pydeezer](https://github.com/Chr1st-oo/pydeezer)
- téléchargement des mp3 vers le dossier local / playlist, via une interface en ligne de commande (pour authentification, choix du profil, de la playlist)

3. interface web ?
  - serveur / interface web permettant de piloter le lecteur + les téléchargements

## roadmap bis ?

1. lecture de playlists locales (fichiers .m3u), possibilité de basculer d'une playlist à la suivante avec le bouton on/off ?
2. construction de playlist de fichiers locaux par scan d'un dossier
3. interface web pour construire des playlists, depuis fichier local et/ou deezer etc ?

## A regarder ?

- https://pad.p2p.legal/s/rec-machine
- [creation de playlist .m3u avec python](https://github.com/TaylorMonacelli/python-create-m3u-playlist-from-directory/blob/master/main.py), [autre exemple plus complet](https://aavtech.site/2018/08/creating-an-m3u-playlist-with-relative-paths-using-powershell-or-python/)
-

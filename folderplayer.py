"""
lecture d'un dossier de fichiers audio / mp3 via VLC
"""

import os, random, vlc, time

class FolderPlayer:

    def __init__(self, path=None, randomize=True):
        self.vlc = vlc.Instance()
        self.list_player = self.vlc.media_list_player_new()
        if path:
            self.path = path
            self.load_folder(path, randomize)

    def load_folder(self, folder_path, randomize=False):
        files = [ os.path.join(folder_path, f) for f in os.listdir(folder_path) if os.path.isfile(os.path.join(folder_path,f)) and f.endswith('.mp3') ]
        # print(f"found {len(files)} files in {folder_path}")
        self.media_list = self.vlc.media_list_new()
        if randomize:
            random.shuffle(files)
        for f in files:
            self.media_list.add_media(f)
        self.list_player.set_media_list(self.media_list)
        print(f"playlist built with {self.media_list.count()} media")

    def play(self):
        self.list_player.play()
        self.display_title()

    def pause(self):
        self.list_player.pause()

    def play_pause(self):
        if self.list_player.is_playing():
            self.pause()
        else:
            self.play()

    def next(self):
        self.list_player.next()
        self.display_title()

    def previous(self):
        self.list_player.previous()
        self.display_title()

    def stop(self):
        self.list_player.stop()

    def display_title(self):
        player = self.list_player.get_media_player()
        media = player.get_media()
        print(f"playing {media.get_mrl()}")

    def get_volume(self):
        return self.list_player.get_media_player().audio_get_volume()

    def set_volume(self, volume):
        if volume >= 0 and volume <= 100:
            print(f"setting volume to {volume}")
            self.list_player.get_media_player().audio_set_volume(volume)

    def volume_up(self, step=10):
        self.set_volume(self.get_volume() + step)

    def volume_down(self, step=10):
        self.set_volume(self.get_volume() - step)

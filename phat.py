"""
surcouche phatbeat pour un player audio
"""

import phatbeat, signal, colorsys
from folderplayer import FolderPlayer

class phatHandler:

    def __init__(self, player, brightness=0.5):
        self.player = player
        self.brightness = brightness
        self.set_volume(3) # 0-8
        self.player.play()

    def play_pause(self, pin):
        self.player.play_pause()

    def next(self, pin):
        self.player.next()

    def previous(self, pin):
        self.player.previous()

    def set_volume(self, level=3):
        if level >= 0 and level <= 8:
            self.volume = level
            self.player.set_volume(self.volume*12) # 0-96
            self.show_volume(level)

    def volume_up(self, pin):
        self.set_volume(self.volume + 1)

    def volume_down(self, pin):
        self.set_volume(self.volume - 1)

    def show_volume(self, level, saturation=1.0, value=0.2):
        phatbeat.clear()
        for i in range(0,8):
            # definition de la teinte en fonction de la "hauteur" du pixel
            # pour un effet "arc en ciel"
            # on divise 360° par 8 "crans" (=45°)
            # et on decale arbitrairement de 45° pour ne pas débuter sur rouge
            hue = 315 - 45*i # definition de la teinte (8 crans = 45°)
            if level > i:
                r, g, b = [int(x * 255) for x in colorsys.hsv_to_rgb(hue / 360.0, saturation, value)]
                phatbeat.set_pixel(7-i, r, g, b, 0.1, channel=0)
                phatbeat.set_pixel(7-i, r, g, b, 0.2, channel=1)
        phatbeat.show()

try:
    print("hell yeah rock'n'roll")
    fp = FolderPlayer("/home/jducastel/music")
    p = phatHandler(fp)
    # attaching buttons events to methods
    phatbeat.on(phatbeat.BTN_PLAYPAUSE, p.play_pause)
    phatbeat.on(phatbeat.BTN_FASTFWD, p.next)
    phatbeat.on(phatbeat.BTN_REWIND, p.previous)
    phatbeat.on(phatbeat.BTN_VOLUP, p.volume_up)
    phatbeat.on(phatbeat.BTN_VOLDN, p.volume_down)
    # start waiting for button events
    signal.pause()
except KeyboardInterrupt:
    print("bye !")
    exit()

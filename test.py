import phatbeat, signal, math
from folderplayer import FolderPlayer

player = FolderPlayer("/home/jducastel/music")

def show_volume(volume):
    phatbeat.clear()
    level = math.floor(volume/8); print(f"level {level}")
    for i in range(0,8): # resetting
        if level > i:
            phatbeat.set_pixel(7-i, 128,128,128,channel=0)
            phatbeat.set_pixel(7-i, 128,128,128,channel=1)
    phatbeat.show()

@phatbeat.on(phatbeat.BTN_FASTFWD)
def fast_forward(pin):
    global player
    player.next()


# @phatbeat.hold(phatbeat.BTN_FASTFWD, hold_time=2)
# def hold_fast_forward(pin):
#     print("FF Held")


@phatbeat.on(phatbeat.BTN_PLAYPAUSE)
def play_pause(pin):
    global player
    player.play_pause()


# @phatbeat.hold(phatbeat.BTN_PLAYPAUSE, hold_time=2)
# def hold_play_pause(pin):
#     print("PP Held")


@phatbeat.on(phatbeat.BTN_VOLUP)
def volume_up(pin):
    global player
    player.volume_up(12)
    show_volume(player.get_volume())

# @phatbeat.hold(phatbeat.BTN_VOLUP)
# def hold_volume_up(pin):
#     print("VU Held")


@phatbeat.on(phatbeat.BTN_VOLDN)
def volume_down(pin):
    global player
    player.volume_down(12)
    show_volume(player.get_volume())

# @phatbeat.hold(phatbeat.BTN_VOLDN)
# def hold_volume_down(pin):
#     print("VD Held")


@phatbeat.on(phatbeat.BTN_REWIND)
def rewind(pin):
        global player
        player.previous()


# @phatbeat.hold(phatbeat.BTN_REWIND)
# def hold_rewind(btn):
#     print("RR Held")


# @phatbeat.on(phatbeat.BTN_ONOFF)
# def onoff(pin):
#     print("OO Pressed")
#
#
# @phatbeat.hold(phatbeat.BTN_ONOFF)
# def hold_onoff(pin):
#     print("OO Held")

try:
    player.play()
    player.set_volume(24)
    show_volume(player.get_volume())
    signal.pause()
except KeyboardInterrupt:
    print("bye !")
    exit()
